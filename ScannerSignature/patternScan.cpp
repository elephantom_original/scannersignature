#include "patternScan.h"

void * patternScan(char* base, size_t size, char* pattern, char* mask) {
	size_t patternLength = strlen(mask);

	for (unsigned int i = 0; i < size - patternLength; i++) {
		bool found = true;

		for (unsigned int j = 0; j < patternLength; j++) {
			if (mask[j] != '?' && pattern[j] != *(base + i + j)) {
				found = false;
				break;
			}
		}

		if (found) return (void*)(base + i);
	}

	return nullptr;
}

void * patternScanEx(HANDLE hProc, uintptr_t start, uintptr_t end, char* pattern, char* mask) {
	uintptr_t currentChunk = start;
	SIZE_T bytesRead;

	while (currentChunk < end) {
		char buffer[4096];

		DWORD protect;
		VirtualProtectEx(hProc, (void*)currentChunk, sizeof(buffer), PAGE_EXECUTE_READWRITE, &protect);
		ReadProcessMemory(hProc, (void*)currentChunk, &buffer, sizeof(buffer), &bytesRead);
		VirtualProtectEx(hProc, (void*)currentChunk, sizeof(buffer), protect, &protect);

		if (bytesRead == 0) return nullptr;

		void* internalAddress = patternScan((char*)&buffer, bytesRead, pattern, mask);

		if (internalAddress != nullptr) {
			uintptr_t offsetFromBuffer = (uintptr_t)internalAddress - (uintptr_t)&buffer;
			return (void*)(currentChunk + offsetFromBuffer);
		}
		else {
			currentChunk = currentChunk + bytesRead;
		}
	}
	return nullptr;
}

void * patternScanExModule(HANDLE hProc, wchar_t * appName, wchar_t * moduleName, char* pattern, char* mask) {
	DWORD procID = getProcID(appName);
	MODULEENTRY32 moduleEntry = getModule(procID, moduleName);

	if (!moduleEntry.th32ModuleID) {
		return nullptr;
	}

	uintptr_t start = (uintptr_t)moduleEntry.modBaseAddr;
	uintptr_t end = start + moduleEntry.modBaseSize;
	return patternScanEx(hProc, start, end, pattern, mask);
}