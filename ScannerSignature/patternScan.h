#pragma once
#include <Windows.h>
#include <TlHelp32.h>
#include "pTools.h"

void * patternScan(char* base, size_t size, char* pattern, char* mask);

void * patternScanEx(HANDLE hProc, uintptr_t start, uintptr_t end, char* pattern, char* mask);

void * patternScanExModule(HANDLE hProc, wchar_t * appName, wchar_t * moduleName, char* pattern, char* mask);