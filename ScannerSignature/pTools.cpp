#include "pTools.h";

// �������� ID �������� �� ����� ����������
DWORD getProcID(wchar_t * appName) {
	PROCESSENTRY32 pEntry = { 0 };
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);

	if (!hSnapshot) return 0;

	pEntry.dwSize = sizeof(pEntry);

	if (!Process32First(hSnapshot, &pEntry)) return 0;

	do {
		// wcscmp ���������� 0, ���� ������������������ ���������
		if (!wcscmp(pEntry.szExeFile, appName)) {
			CloseHandle(hSnapshot);
			return pEntry.th32ProcessID;
		}
	} while (Process32Next(hSnapshot, &pEntry));

	CloseHandle(hSnapshot);
	return 0;
}

// �������� ������ �� ��� �����
MODULEENTRY32 getModule(DWORD procID, wchar_t * moduleName) {
	MODULEENTRY32 moduleEntry = { 0 };
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE | TH32CS_SNAPMODULE32, procID);

	if (hSnapshot != INVALID_HANDLE_VALUE) {
		MODULEENTRY32 current = { 0 };
		current.dwSize = sizeof(current);

		if (Module32First(hSnapshot, &current)) {
			do {
				// wcscmp ���������� 0, ���� ������������������ ���������
				if (!wcscmp(current.szModule, moduleName)) {
					moduleEntry = current;
					break;
				}
			} while (Module32Next(hSnapshot, &current));
		}
		CloseHandle(hSnapshot);
	}
	return moduleEntry;
}